<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */


class Func_Xslt extends bab_functionality 
{
	protected $xml;
	protected $xsl;
	
	public function getDescription()
	{
		return 'Xslt, transform xml document to HTML with a XSL stylesheet';
	}
	
	/**
	 * Transform a dom document
	 */
	public function setDOMDocument(DOMDocument $xml)
	{
		$this->xml = $xml;
	}
	
	/**
	 * Transform an ovml datasource file
	 * @param	string	$filename		A relative path to ovidentia root
	 */	
	public function setDatasource($filename)
	{
		$path = realpath(dirname('.').'/'.$filename);
		
		$datasource = bab_functionality::get('Ovml/Datasource');
		/*@var $datasource Func_Ovml_Datasource */
		$datasource->setDatasourceFile($path);
		
		$this->setDOMDocument($datasource->getDomDocument());
	}
	
	
	/**
	 * 
	 * @param string $filename		A relative path to ovidentia root
	 */
	public function setStyleSheet($filename)
	{
		$path = realpath(dirname('.').'/'.$filename);
		
		$this->xsl = DomDocument::load($path);
	}
	
	public function getXml()
	{
		$xslt = new xsltProcessor;
		$xslt->importStyleSheet($this->xsl);
		$str = $xslt->transformToXML($this->xml);
		
		return bab_getStringAccordingToDataBase($str, 'UTF-8');
	}
}