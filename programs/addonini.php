;<?php /*

[general]
name							="xslt"
version							="0.1"
addon_type						="EXTENSION"
encoding						="UTF-8"
mysql_character_set_database	="latin1"
description						="a controller to use the ovml with xml or json files"
description.fr					="un controlleur permettant d'utiliser l'API ovml avec des fichiers XML ou JSON"
delete							="1"
ov_version						="8.1.94"
php_version						="5.2.0"
addon_access_control			="0"
author							="Paul de Rosanbo"
mysql_character_set_database	="latin1,utf8"
mod_dom							="Available"
mod_xsl							="Available"

[addons]
ovmldatasource					="0.1"

;*/