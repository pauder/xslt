<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
   <xsl:apply-templates select="ArticleTopic/ContainerItem" />
   <div class="content">
       <xsl:apply-templates />
   </div>
</xsl:template>

<xsl:template match="ArticleTopic/ContainerItem">
    <h1><xsl:value-of select="TopicName" /></h1>
</xsl:template>

<xsl:template match="Articles/ContainerItem">
  <div style="width:200px; display:inline-block;text-align:center; border:#ccc 2px solid; margin:.5em;min-height:200px;">
    <p><strong class="title"><xsl:value-of select="ArticleTitle" /></strong></p>
    <div class="head">
        <img src="{ImageUrl}" alt="{ArticleTitle}" style="padding:5px;" /><br/>
        <xsl:value-of select="ArticleHead" />
    </div>
  </div>
</xsl:template>



</xsl:stylesheet>